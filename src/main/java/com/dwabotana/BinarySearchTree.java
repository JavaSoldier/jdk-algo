package com.dwabotana;

// Java program to demonstrate insert operation in binary search tree
public class BinarySearchTree {

    // Root of BST
    Node root;

    public BinarySearchTree() {
        root = null;
    }

    private static void printLeafNodes(Node node) {
        // base case
        if (node == null) {
            return;
        }
        if (node.left == null && node.right == null) {
            System.out.printf("%d ", node.key);
        }
        printLeafNodes(node.left);
        printLeafNodes(node.right);
    }

    public int search(int key) {
        return this.search(root, key);
    }

    private Integer search(Node node, int key) {
        if (node.key == key) {
            return node.key;
        } else if (node.key < key) {
            return search(node.right, key);
        } else if (node.key > key) {
            return search(node.left, key);
        }
        return null;
    }

    public void insert(int key) {
        root = insert(root, key);
    }

    public void print() {
        this.print(root);
    }

    /*prints recursive tree into the console todo add separation left and right using spaces*/
    private void print(Node node) {
        //find space count before element. we need to find all child nodes every left will add 2 spaces if right between left remove 2 space

        System.out.println(node.key);
        if (node.left != null) {
            System.out.println("/");
            print(node.left);
        }
        if (node.right != null) {
            System.out.println("\\");
            print(node.right);
        }
    }

    public void printLeafNodes() {
        this.printLeafNodes(root);
    }

    private void iterativeStylePrint(Node node) {

        while (node.left != null) {

        }
    }

    /* A recursive function to insert a new key in BST */
    private Node insert(Node root, int key) {
        /* If the tree is empty, return a new node */
        if (root == null) {
            root = new Node(key);
            return root;
        }
        /* Otherwise, recur down the tree */
        if (key < root.key)
            root.left = insert(root.left, key);
        else if (key > root.key)
            root.right = insert(root.right, key);
        /* return the (unchanged) node pointer */
        return root;
    }

    /* recursive structure containing left and right child of current node and key value*/
    private class Node {
        int key;
        Node left, right;

        Node(int item) {
            key = item;
            left = right = null;
        }
    }
}
