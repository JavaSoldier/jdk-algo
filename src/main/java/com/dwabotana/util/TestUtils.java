package com.dwabotana.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class TestUtils {

    private int size;
    private Integer maxValue;

    public TestUtils(int size) {
        this.size = size;
    }

    /*arguments mus be equal if using DStructure that not allow duplicates*/
    public TestUtils(int size, int maxValue) {
        this.size = size;
        this.maxValue = maxValue;
    }

    public static BigDecimal calculateTimeDiff(long start, long finish) {
        BigDecimal startTime = BigDecimal.valueOf(start);
        BigDecimal finishTime = BigDecimal.valueOf(finish);
        return (finishTime.subtract(startTime)).divide(BigDecimal.valueOf(1000000L), BigDecimal.ROUND_HALF_EVEN);
    }

    public int getSize() {
        return size;
    }

    public Integer getMaxValue() {
        return maxValue;
    }

    public void printList(List<Integer> list) {
        list.forEach(System.out::println);
    }

    public Collection<Integer> fillIncreasingIntCollection(Collection<Integer> collection) {
        initializeCollection(collection, true);
        return collection;
    }

    public Collection<Integer> fillDecreasingIntCollection(Collection<Integer> collection) {
        initializeCollection(collection, false);
        return collection;
    }

    public Collection<Integer> fillRandomOrderIntCollection(Collection<Integer> collection) {
        initializeCollection(collection, null);
        return collection;
    }

    public Map<Integer, Integer> initializeMap(Map<Integer, Integer> map) {
        int i = 0;
        while (map.size() != size) {
            map.put(++i, i);
        }
        return map;
    }

    private void initializeCollection(Collection<Integer> list, Boolean increase) {
        if (increase == null) {
            Random random = new Random();
            //add random
            if (maxValue == null) {
                while (list.size() != size) {
                    list.add(random.nextInt());
                }
            } else {
                while (list.size() != size) {
                    list.add(random.nextInt(maxValue));
                }
            }

        } else if (increase) {
            int i = 0;
            //add increasing order
            while (list.size() != size) {
                list.add(++i);
            }
        } else {
            int i = size;
            //add decreasing order
            while (list.size() != size) {
                list.add(i--);
            }
        }
    }
}
