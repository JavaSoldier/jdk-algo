package com.dwabotana.util;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ArrayInitUtils {

    public BigDecimal sortTime1k;
    public BigDecimal sortTime10k;
    public BigDecimal sortTime100k;
    public BigDecimal sortTime1m;
//    public BigDecimal sortTime10m;
//    public BigDecimal sortTime100m;

    public int[] arr_1k = new TestUtils(1000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();
    public int[] arr_10k = new TestUtils(10000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();
    public int[] arr_100k = new TestUtils(100000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();
    public int[] arr_1m = new TestUtils(1000000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();
//    public int[] arr_10m = new TestUtils(10000000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();
//    public int[] arr_100m = new TestUtils(100000000).fillRandomOrderIntCollection(new ArrayList<>()).stream().mapToInt(xuy -> xuy).toArray();

    private ArrayInitUtils() {
    }

    public static ArrayInitUtils build() {
        return new ArrayInitUtils();
    }
}
