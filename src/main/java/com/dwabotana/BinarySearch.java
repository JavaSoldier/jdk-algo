package com.dwabotana;

public class BinarySearch {

    public static int search(int key, Integer[] arr) {
        return search(key, arr, 0, arr.length);
    }

    private static int search(Integer key, Integer[] arr, int lo, int hi) {
        if (hi <= lo) return -1;// fast validation for bad input and result if no element
        int mid = lo + (hi - lo) / 2;// find mid at all conditions
        int cmp_result = key.compareTo(arr[mid]);
        if (cmp_result < 0) {
            return search(key, arr, lo, mid);
        } else if (cmp_result > 0) {
            return search(key, arr, mid + 1, hi);
        } else {
            return mid;
        }
    }
}
