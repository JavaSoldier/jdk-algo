package com.dwabotana;

public class MergeSort {

    public void sort(int[] arr) {
        sort(arr, arr.length);
    }

    private void sort(int[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        int[] l = new int[mid];
        int[] r = new int[n - mid];

        System.arraycopy(a, 0, l, 0, mid);
        System.arraycopy(a, mid, r, 0, a.length - mid);


        sort(l, mid);
        sort(r, n - mid);

        merge(a, l, r);
    }

    private void merge(int[] a, int[] l, int[] r) {

        int leftIncrement = 0, rightIncrement = 0, arrayIncrement = 0;

        while (leftIncrement < l.length && rightIncrement < r.length) {

            if (l[leftIncrement] <= r[rightIncrement])
                a[arrayIncrement++] = l[leftIncrement++];
            else
                a[arrayIncrement++] = r[rightIncrement++];

        }
        //handle
         while (leftIncrement < l.length)
            a[arrayIncrement++] = l[leftIncrement++];

         while (rightIncrement < r.length)
            a[arrayIncrement++] = r[rightIncrement++];
    }
}
