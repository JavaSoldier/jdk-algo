package com.dwabotana;

public class MyFastSort {

    public void sort(int[] arr) {
        if (arr.length == 0 || arr.length == 1) {
            return;//0 and 1 element size arr is already sorted
        } else if (arr.length == 2) {
            if (arr[0] > arr[1]) {
                int tmp = arr[1];
                arr[1] = arr[0];
                arr[0] = tmp;
            }
            return;
        } else {
            sort(arr, 0, arr.length - 1);
        }
    }


    private void sort(int[] arr, int beginIndex, int endIndex) {
        int pivotIndex = beginIndex + (endIndex - beginIndex) / 2;
        if (beginIndex < endIndex) {
        swap(arr, pivotIndex, beginIndex, endIndex);

            sort(arr, beginIndex, pivotIndex);
            sort(arr, pivotIndex + 1, endIndex);
        }
    }

//    private void sort(int[] arr, int beginIndex, int endIndex) {
//        int pivotIndex = beginIndex + (endIndex - beginIndex) / 2;
//        if (beginIndex < endIndex) {//sort elements based on pivot
//            for (int i = beginIndex; i < pivotIndex; i++) {
//                if (arr[i] > arr[pivotIndex]) {
//                    bubbleUpOneElement(arr, pivotIndex, endIndex, i, pivotIndex);
//                }
//            }
//            for (int i = endIndex; i > pivotIndex; i--) {
//                if (arr[i] < arr[pivotIndex]) {
//                    bubbleDownOneElement(arr, beginIndex, pivotIndex, i, pivotIndex);
//                }
//            }
//            sort(arr, beginIndex, pivotIndex);
//            sort(arr, pivotIndex + 1, endIndex);
//        }
//    }

//    private void sort(int[] arr, int beginIndex, int endIndex) {
//        int pivotIndex = beginIndex + (endIndex - beginIndex) / 2;
//        if (beginIndex < endIndex) {
//            for (int i = beginIndex; i < pivotIndex; i++) {
//                if (arr[i] > arr[pivotIndex]) {//find a place for arr[i]
//                    for (int j = i; j <= endIndex; j++) {
//                        if (j > pivotIndex) {
//                            break;
//                        }
//                        if (arr[i] > arr[j] && arr[i] > arr[pivotIndex]) {
//                            int tmp = arr[j];
//                            arr[j] = arr[i];
//                            arr[i] = tmp;
//                        }
//                    }
//                }
//            }
//            for (int i = endIndex; i > pivotIndex; i--) {
//                if (arr[i] < arr[pivotIndex]) {
//                    for (int j = i; j >= beginIndex; j--) {
//                        if (arr[i] < arr[j]) {
//                            int tmp = arr[j];
//                            arr[j] = arr[i];
//                            arr[i] = tmp;
//                        }
//                    }
//                }
//            }
//            sort(arr, beginIndex, pivotIndex);
//            sort(arr, pivotIndex + 1, endIndex);
//        }
//    }

    /*fixme todo still not work on worst case - cannot find a place 4 -10*/
    private void swap(final int[] arr, int pivotIndex, int beginIndex, int endIndex) {
        int afterPivot = pivotIndex;
        int beforePivot = pivotIndex;
        int beginIndexCopy = beginIndex;
        int endIndexCopy = endIndex;
        while (beginIndexCopy < pivotIndex) {
            if (arr[beginIndexCopy] > arr[pivotIndex] && arr[afterPivot] < arr[pivotIndex]) {
                // perfect condition swap elements in both side of pivot
                int tmp = arr[beginIndexCopy];
                arr[beginIndexCopy] = arr[afterPivot];
                arr[afterPivot] = tmp;
                beginIndexCopy++;
            } else if (afterPivot == endIndexCopy) {
                beginIndexCopy++;//we don`t find element 4 exchange lets take new one
                afterPivot = pivotIndex + 1;//we don`t find element 4 exchange
            } else {
                afterPivot++;//we don`t find element 4 exchange lets take new pretender from other side
            }
        }
        if (pivotIndex!=0 && arr[pivotIndex-1]>arr[pivotIndex]) {
            int tmp = arr[pivotIndex];
            arr[pivotIndex]=arr[pivotIndex-1];
            arr[pivotIndex-1]=tmp;
        }
        beginIndexCopy = beginIndex;
        endIndexCopy = endIndex;
        while (endIndexCopy > pivotIndex) {
            if (arr[endIndexCopy] < arr[pivotIndex] && arr[beforePivot] > arr[pivotIndex]) {
                // swap elements in both side of pivot
                int tmp = arr[endIndexCopy];
                arr[endIndexCopy] = arr[beforePivot];
                arr[beforePivot] = tmp;
                endIndexCopy--;
            } else if (beforePivot == beginIndexCopy) {
                beforePivot = pivotIndex - 1;
                endIndexCopy--;//we don`t find element 4 exchange lets take new one
            } else {
                beforePivot--;//we dont find element 4 exchange lets take new pretender from other side
            }
        }
        if(pivotIndex!=endIndex && arr[pivotIndex+1]<arr[pivotIndex]){
            int tmp = arr[pivotIndex];
            arr[pivotIndex]=arr[pivotIndex+1];
            arr[pivotIndex+1]=tmp;
        }
    }

//    private void bubbleUpOneElement(final int[] arr, int beginIndex, int endIndex, int elementIndex, int pivotIndex) {
//        while (beginIndex <= endIndex) {
//            if (arr[elementIndex] > arr[beginIndex] && arr[beginIndex] < arr[pivotIndex]) {
//                int tmp = arr[beginIndex];
//                arr[beginIndex] = arr[elementIndex];
//                arr[elementIndex] = tmp;
//                return;
//            } else if (beginIndex == endIndex && arr[beginIndex] > arr[pivotIndex]) {
//                int elementTmp = arr[elementIndex];
//                arr[elementIndex] = arr[pivotIndex];
//                arr[pivotIndex] = arr[endIndex];
//                arr[endIndex] = elementTmp;
//                return;
//            } else {
//                beginIndex++;
//            }
//        }
//    }
//
//    private void bubbleDownOneElement(final int[] arr, int beginIndex, int endIndex, int elementIndex, int pivotIndex) {
//        while (beginIndex <= endIndex) {
//            if (arr[elementIndex] < arr[endIndex] && arr[endIndex] > arr[pivotIndex]) {
//                int tmp = arr[endIndex];
//                arr[endIndex] = arr[elementIndex];
//                arr[elementIndex] = tmp;
//                return;
//            } else if (beginIndex == endIndex && arr[endIndex] < arr[pivotIndex]) {
//                //swap if stuck in situation like [1,2,-1]
//                int elementTmp = arr[elementIndex];
//                arr[elementIndex] = arr[pivotIndex];
//                arr[pivotIndex] = arr[endIndex];
//                arr[endIndex] = elementTmp;
//                return;
//            } else {
//                endIndex--;
//            }
//        }
//
//    }
}
