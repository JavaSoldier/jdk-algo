package com.dwabotana;

public class BubbleSort {

    public void sort(int arr[]) {
        for (int counterFromEnd = arr.length - 1; counterFromEnd >= 0; counterFromEnd--) {
            // optimized -> each iteration brings biggest element to the end -> so we can reduce inner loop iteration count
            for (int counterFromBeginning = 0; counterFromBeginning < counterFromEnd && arr[counterFromBeginning] > arr[counterFromBeginning + 1]; counterFromBeginning++) {
                int temp = arr[counterFromBeginning];
                arr[counterFromBeginning] = arr[counterFromBeginning + 1];
                arr[counterFromBeginning + 1] = temp;
            }
        }
    }
}