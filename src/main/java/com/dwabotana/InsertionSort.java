package com.dwabotana;

/*tuning version of bubble sort - all optimization done in inner loop*/
public class InsertionSort {

    public void sort(int arr[])
    {
        if(arr.length<2){
            // less than 2 elements array is already sorted :)
            return;
        }
        int n = arr.length;
        for (int arrCounterIndex = 1; arrCounterIndex < n; ++arrCounterIndex) {
            int key = arr[arrCounterIndex];
            int elementIndex = arrCounterIndex - 1;

            /* Move elements of arr[0..arrCounterIndex-1], that are
               greater than key, to one position ahead
               of their current position */
            while (elementIndex >= 0 && arr[elementIndex] > key) {
                arr[elementIndex + 1] = arr[elementIndex];
                elementIndex = elementIndex - 1;
            }
            arr[elementIndex + 1] = key;
        }
    }

// len+(n-1){
//              arr[?]+2*varInit+ariphOp+ N/2( 2*compare+arr[?])
//                  {
//                            2*arr[?]+ariphOp
//                  }
//                arr[?]+ariphOP
//          }
}
