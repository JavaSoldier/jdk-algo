package com.dwabotana;

public class FastSort {

    public void sort(int[] arr) {
        this.sort(arr, 0, arr.length - 1);
    }

    private void sort(int arr[], int begin, int end) {
        if (begin < end) {
            int partitionIndex = partition(arr, begin, end);

            sort(arr, begin, partitionIndex-1);
            sort(arr, partitionIndex+1, end);
        }
    }

    private int partition(int arr[], int begin, int end) {
        int pivot = arr[end];
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            if (arr[j] <= pivot) {
                i++;

                int swapTemp = arr[i];
                arr[i] = arr[j];
                arr[j] = swapTemp;
            }
        }

        int swapTemp = arr[i+1];
        arr[i+1] = arr[end];
        arr[end] = swapTemp;

        return i+1;
    }

//    private int partitionCopy(int arr[], int begin, int end) {
//        int pivotVal = arr[end];
//        int beginMinusOneIndex = (begin - 1);
//        for (int beginIndex = begin; beginIndex < end; beginIndex++) {
//            if (arr[beginIndex] <= pivotVal) {
//                beginMinusOneIndex++;
//
//                int swapTemp = arr[beginMinusOneIndex];
//                arr[beginMinusOneIndex] = arr[beginIndex];
//                arr[beginIndex] = swapTemp;
//            }
//        }
//        int swapTemp = arr[beginMinusOneIndex + 1];
//        arr[beginMinusOneIndex + 1] = arr[end];
//        arr[end] = swapTemp;
//        return beginMinusOneIndex + 1;
//    }
}
