package com.dwabotana.algo;

import com.dwabotana.BinarySearchTree;

public class BinarySearchTreeTest {

    public static void main(String[] args) {
        BinarySearchTree tree = new BinarySearchTree();

    /* Let us create following BST
             50
           /    \
          30     70
         /  \   /  \
       20   40 60   80 */

        tree.insert(20);
        tree.insert(50);
        tree.insert(30);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);

        tree.print();
        System.out.println("-------------- divider ------------> ");
        System.out.println(tree.search(40));
        System.out.println("-------------- divider ------------> ");
        tree.printLeafNodes();
    }
}
