package com.dwabotana.algo;

import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

public class SortTest {

    private TestUtils util = new TestUtils(1000000);
    private ArrayList<Integer> list;

    @Before
    public void init() {
        list = (ArrayList<Integer>) util.fillDecreasingIntCollection(new ArrayList<>());
    }

    @Test
    public void testCollectionSorts() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------sorting-------------");
        Collections.sort(list);
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("sort time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000)));

    }
}
