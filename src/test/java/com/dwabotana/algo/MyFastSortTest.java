package com.dwabotana.algo;

import com.dwabotana.MyFastSort;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;

@Ignore
public class MyFastSortTest {

    //    private static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();
    MyFastSort fast = new MyFastSort();

    @Test
    public void test4DebugEasyCase() {
        int arr[] = new int[]{1, 2, 3, 2, 6, 5, -1};
        fast.sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void test4DebugBestCase() {
        int arr[] = new int[]{1, 2, 4, 3, 5, 6, 8, 7, 9, 10, 12, 11, 14, 13, 15, 20, 19, 18, 17};
        fast.sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void test4DebugWorstCase() {
        int arr[] = new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1, -1, -2, -3, -4, -5, -6, -7, -8, -9, -10};
        fast.sort(arr);
        System.out.println(Arrays.toString(arr));
    }

//    @AfterClass
//    public static void testSpeed() {
//        System.out.println("my fast sort test 1k 10k 100k 1m is not supported - so fuckdem slow");
//        System.out.println(arrayInitUtils.sortTime1k);
//        System.out.println(arrayInitUtils.sortTime10k);
//        System.out.println(arrayInitUtils.sortTime100k);
//        System.out.println(arrayInitUtils.sortTime1m);
//        assertTrue("must be more than 60", (arrayInitUtils.sortTime10k.divide(arrayInitUtils.sortTime1k, BigDecimal.ROUND_CEILING).compareTo(BigDecimal.valueOf(60L)) > 0));
//        assertTrue("must be more than 100", (arrayInitUtils.sortTime100k.divide(arrayInitUtils.sortTime10k, BigDecimal.ROUND_CEILING).compareTo(BigDecimal.valueOf(100L)) > 0));
//    }

//    @Test
//    public void testSort1k() {
//        long start = System.nanoTime();
//        fast.sort(arrayInitUtils.arr_1k);
//        long finish = System.nanoTime();
//        arrayInitUtils.sortTime1k = TestUtils.calculateTimeDiff(start, finish);
//    }

//    @Test
//    public void testSort10k() {
//        long start = System.nanoTime();
//        fast.sort(arrayInitUtils.arr_10k);
//        long finish = System.nanoTime();
//        arrayInitUtils.sortTime10k = TestUtils.calculateTimeDiff(start, finish);
//    }

//    @Test
//    public void testSort100k() {
//        long start = System.nanoTime();
//        fast.sort(arrayInitUtils.arr_100k);
//        long finish = System.nanoTime();
//        arrayInitUtils.sortTime100k = TestUtils.calculateTimeDiff(start, finish);
//    }
}
