package com.dwabotana.algo;

import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

public class RoutingDataManipulationTest {

    private TestUtils util = new TestUtils(1000);
    private ArrayList<Integer> list;

    @Before
    public void init() {
        // todo specify list element order
        list = (ArrayList<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
    }

    @Test
    public void testReverse() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------reverse-------------");
        Collections.reverse(list);
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("reverse time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000)));
    }

    @Test
    public void testFill() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------fill-------------");
        Collections.fill(list, 111);
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("fill time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000)));
    }

    @Test
    public void testCopy() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------copy-------------");
        Collections.copy(list, (ArrayList<Integer>) util.fillDecreasingIntCollection(new ArrayList<>()));
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("copy time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000)));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testCopyMoreThanSize() {
        ArrayList<Integer> combined = (ArrayList<Integer>) new TestUtils(100000).fillRandomOrderIntCollection(new ArrayList<>());
        BigDecimal startTime = BigDecimal.valueOf(System.nanoTime());
        System.out.println("--------------copy-------------");
        Collections.copy(list, combined);
        BigDecimal finishTime = BigDecimal.valueOf(System.nanoTime());
        System.out.println("copy time is " + (finishTime.subtract( startTime)).divide(BigDecimal.valueOf(1000000000), BigDecimal.ROUND_HALF_EVEN));
    }

    @Test
    public void testCopyLessThanSize() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------copy-------------");
        Collections.copy(list, (ArrayList<Integer>) new TestUtils(10).fillRandomOrderIntCollection(new ArrayList<>()));
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("copy time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000), BigDecimal.ROUND_HALF_EVEN));
    }

    @Test
    public void testSwap() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------swap-------------");
        Collections.swap(list, 0, list.size() - 1);
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("swap time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000), BigDecimal.ROUND_HALF_EVEN));
    }

    @Test
    public void testAddAll() {
        util.printList(list);
        Integer[] arr = new Integer[]{};
        arr = util.fillRandomOrderIntCollection(new ArrayList<>()).toArray(arr);
        long startTime = System.nanoTime();
        System.out.println("--------------addAll-------------");
        Collections.addAll(list, arr);
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("addAll time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000), BigDecimal.ROUND_HALF_EVEN));
    }

    @Test
    public void testAdd() {
        util.printList(list);
        long startTime = System.nanoTime();
        System.out.println("--------------addAll-------------");
        list.addAll(util.fillRandomOrderIntCollection(new ArrayList<>()));
        long finishTime = System.nanoTime();
        util.printList(list);
        System.out.println("addAll time is " + new BigDecimal(finishTime - startTime).divide(BigDecimal.valueOf(1000000000)));
    }
}
