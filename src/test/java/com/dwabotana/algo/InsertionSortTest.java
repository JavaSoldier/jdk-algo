package com.dwabotana.algo;

import com.dwabotana.InsertionSort;
import com.dwabotana.util.ArrayInitUtils;
import com.dwabotana.util.TestUtils;
import org.junit.Test;

import java.util.Arrays;

public class InsertionSortTest {

    static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();
    InsertionSort insertionSort = new InsertionSort();

    @Test
    public void test4DebugEasyCase() {
        int arr[] = new int[]{2, 1, 9, 6, 5, 10, 9, 8, -10};
        insertionSort.sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void testSort1k() {
        insertionSort.sort(arrayInitUtils.arr_1k);
    }

    @Test
    public void testSort10k() {
        insertionSort.sort(arrayInitUtils.arr_10k);
    }

    @Test
    public void testSort100k() {
        insertionSort.sort(arrayInitUtils.arr_100k);
    }

    //    @Test too long time but more less than bubble sort. why it almost the same
    public void testSort1m() {
        long start = System.nanoTime();
        insertionSort.sort(arrayInitUtils.arr_1m);
        long finish = System.nanoTime();
        arrayInitUtils.sortTime1m = TestUtils.calculateTimeDiff(start, finish);
    }
}
