package com.dwabotana.algo;

import com.dwabotana.BubbleSort;
import com.dwabotana.util.ArrayInitUtils;
import org.junit.Test;

public class BubbleSortTest {

    static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();
    BubbleSort bubbleSort = new BubbleSort();

//    @Test
//    public void test4Debug(){
//        int [] arr = new int[]{2,1,9,6,5,10,9,8,-10};
//        bubbleSort.sort(arr);
//        System.out.println(Arrays.toString(arr));
//    }

    @Test
    public void testSort1k() {
        bubbleSort.sort(arrayInitUtils.arr_1k);
    }

    @Test
    public void testSort10k() {
        bubbleSort.sort(arrayInitUtils.arr_10k);
    }

    @Test
    public void testSort100k() {
        bubbleSort.sort(arrayInitUtils.arr_100k);
    }
}
