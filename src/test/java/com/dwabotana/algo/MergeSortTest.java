package com.dwabotana.algo;

import com.dwabotana.MergeSort;
import com.dwabotana.util.ArrayInitUtils;
import org.junit.Test;

import java.util.Arrays;

public class MergeSortTest {

    static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();
    private MergeSort mergeSort = new MergeSort();

    @Test
    public void testBestCase() {
        int[] arr = {10, 6, 8, 5, 7, 3, 4};
        mergeSort.sort(arr);
        System.out.println(Arrays.toString(arr));
    }

    @Test
    public void testSort1k() {
        mergeSort.sort(arrayInitUtils.arr_1k);
    }

    @Test
    public void testSort10k() {
        mergeSort.sort(arrayInitUtils.arr_10k);
    }

    @Test
    public void testSort100k() {
        mergeSort.sort(arrayInitUtils.arr_100k);
    }

    @Test
    public void testSort1m() {
        mergeSort.sort(arrayInitUtils.arr_1m);
    }
}
