package com.dwabotana.algo;

import com.dwabotana.MyGolovachQuickSort;
import com.dwabotana.util.ArrayInitUtils;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: GLEB
 * Date: 09.05.14
 * Time: 21:18
 * To change this template use File | Settings | File Templates.
 */

public class MyGolovachQuickSortTest {

    private static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();

    MyGolovachQuickSort quick = new MyGolovachQuickSort();


    @Test
    public void testSort1k() {
        quick.sort(arrayInitUtils.arr_1k);
    }

    @Test
    public void testSort10k() {
        quick.sort(arrayInitUtils.arr_10k);
    }

    @Test
    public void testSort100k() {
        quick.sort(arrayInitUtils.arr_100k);
    }

    @Test
    public void testSort1m() {
        quick.sort(arrayInitUtils.arr_1m);
    }

}