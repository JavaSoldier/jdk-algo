package com.dwabotana.algo;

import com.dwabotana.FastSort;
import com.dwabotana.util.ArrayInitUtils;
import org.junit.Test;

public class FastSortTest {
    private static ArrayInitUtils arrayInitUtils = ArrayInitUtils.build();
    FastSort fast = new FastSort();

//    @Test
//    public void test4DebugEasyCase() {
//        int arr[] = new int[]{5, 9, 4, 6, 5, 3};
//        fast.sort(arr);
//        System.out.println(Arrays.toString(arr));
//    }

    @Test
    public void testSort1k() {
        fast.sort(arrayInitUtils.arr_1k);
    }

    @Test
    public void testSort10k() {
        fast.sort(arrayInitUtils.arr_10k);
    }

    @Test
    public void testSort100k() {
        fast.sort(arrayInitUtils.arr_100k);
    }

    @Test
    public void testSort1m() {
        fast.sort(arrayInitUtils.arr_1m);
    }
}
