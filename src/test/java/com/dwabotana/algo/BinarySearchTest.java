package com.dwabotana.algo;

import com.dwabotana.BinarySearch;
import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class BinarySearchTest {

    private TestUtils util = new TestUtils(1000);
    private ArrayList<Integer> list;

    @Before
    public void init() {
        // todo specify list element order
        list = (ArrayList<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
    }

    @Test
    public void testBinarySearch() {
        System.out.println(BinarySearch.search(11, new Integer[]{1, 2, 3, 4, 5, 111, 112, 113, 222}));
    }

    @Test
    public void testJDKBinarySearch() {
        Integer result = Collections.binarySearch(Arrays.asList(1, 1000, 2, 10, 999, -100, 100500, 758403, -55), -100);
        System.out.println(result);
        System.out.println("incorrect result. that's why list is not sorted");

        Integer result2 = Collections.binarySearch(list, 999);
        System.out.println(result2);
        System.out.println(list.get(998));
    }

    @Test
    public void testJDKBinarySearchReverse() {
        list = (ArrayList<Integer>) util.fillDecreasingIntCollection(new ArrayList<>());
        Integer result = Collections.binarySearch(list, 999);
        System.out.println(result);
        System.out.println("incorrect result. list is sorted but diff order");
        Integer result2 = Collections.binarySearch(list, 999, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        System.out.println(result2);
    }
}
