package com.dwabotana.algo;

import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.*;

public class CompositionMaxMinTest {

    private TestUtils util = new TestUtils(1000);
    private TestUtils util2 = new TestUtils(100);
    private ArrayList<Integer> list1000;
    private ArrayList<Integer> list100;

    @Before
    public void init() {
        // todo specify list element order
        list1000 = (ArrayList<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
        list100 = (ArrayList<Integer>) util2.fillDecreasingIntCollection(new ArrayList<>());
    }

    @Test
    public void testFrequency() {
        System.out.println("--------------Frequency-------------");
        int result = Collections.frequency(list100, 100);
        int result2 = Collections.frequency(list100, 1000);
        int result3 = Collections.frequency(list100, null);
        assertEquals(1, result);
        assertEquals(0, result2);
        assertEquals(0, result3);
        System.out.println("result = " + result + " result2 = " + result2 + " result3 = " + result3);
    }

    @Test
    public void testDisjoint() {
        System.out.println("--------------Disjoint-------------");
        assertFalse("contains same elements", Collections.disjoint(list1000, list100));
        assertTrue("no same elements but depend on random", Collections.disjoint(list100, util.fillRandomOrderIntCollection(new ArrayList<>())));
    }

    @Test
    public void testMax() {
        assertEquals(Integer.valueOf(100), Collections.max(list100));
        assertEquals(Integer.valueOf(1000), Collections.max(list1000));
        System.out.println(Collections.max(util.fillRandomOrderIntCollection(new ArrayList<>())));
    }

    @Test
    public void testMin() {
        assertEquals(Integer.valueOf(1), Collections.min(list100));
        assertEquals(Integer.valueOf(1), Collections.min(list1000));
        System.out.println(Collections.min(util.fillRandomOrderIntCollection(new ArrayList<>())));
    }
}
