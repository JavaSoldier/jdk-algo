package com.dwabotana.algo;

import com.dwabotana.util.TestUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class ShuffleTest {
    TestUtils util = new TestUtils(1000);
    private ArrayList<Integer> list;

    @Before
    public void init() {
        list = (ArrayList<Integer>) util.fillIncreasingIntCollection(new ArrayList<>());
    }

    @Test
    public void testCollectionShuffle() {
        Collections.shuffle(list);
        util.printList(list);
    }
}
